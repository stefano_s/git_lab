close all
clear all
sample_square_dirichlet; % CARICA LA MESH: MATRICI: X Y T e pivot 

P=[X Y];

f=@(x,y) 1; % funzione forzante, "right hand side"

Nele=size(T,1); % Numero di triangoli, pari alla lunghezza della matrice T
Ndof=max(pivot); % numero di gradi di liberta', pari all'indice massimo nel vettore pivot
NNodof=-min(pivot); % numero di gradi di liberta', pari all'indice massimo nel vettore pivot
Nnode=size(P,1); % Nimero di vertoci della triangolazione

% Calcolo dei contributi alla matrice di rigidezza per inizializzare i
% vettori Aval, Ai, Aj
Ncontributes=0;
for e=1:Nele
    NClocali=sum(pivot(T(e,:))>0)^2;
    Ncontributes=Ncontributes+NClocali;
end
% NB: Il calcolo si puo' effettuare senza ciclo for:
%Ncontributes=sum(sum(pivot(T)>0,2).^2);


NcontributesD=sum(sum(pivot(T)<0,2).^2);

Aval=zeros(Ncontributes,1);
Ai=zeros(Ncontributes,1);
Aj=zeros(Ncontributes,1);
Mval=zeros(Ncontributes,1);
Adval=zeros(NcontributesD,1);
Adi=zeros(NcontributesD,1);
Adj=zeros(NcontributesD,1);
b=zeros(Ndof,1);
k=1;
kd=1;
for e=1:Nele
    xg = (P(T(e,1),1) + P(T(e,2),1) + P(T(e,3),1))/3.0; % coordinata x del baricentro del triangolo
    yg = (P(T(e,1),2) + P(T(e,2),2) + P(T(e,3),2))/3.0; % coordinata y del baricentro del triangolo
    Dx(1) = P(T(e,3),1) - P(T(e,2),1);  % Elementi per il calcolo del gradiente
    Dx(2) = P(T(e,1),1) - P(T(e,3),1);
    Dx(3) = P(T(e,2),1) - P(T(e,1),1);
    Dy(1) = P(T(e,2),2) - P(T(e,3),2);
    Dy(2) = P(T(e,3),2) - P(T(e,1),2);
    Dy(3) = P(T(e,1),2) - P(T(e,2),2);
    Area = abs(0.5*(Dx(3)*Dy(2)-Dx(2)*Dy(3))); % Area del triangolo
    for i=1:3 % ciclo sui vertici LOCALI
        Vi=T(e,i); % vertice GLOBALE
        ii=pivot(Vi); % grado di liberta' 
        if (ii>0) % controllo se effettivamente e' grado di liberta'
            for j=1:3 % ciclo sui vertici LOCALI
                Vj=T(e,j); % vertice GLOBALE
                jj=pivot(Vj); % grado di liberta' 
                if (jj>0) % controllo se effettivamente e' grado di liberta'
                    % se entrambi sono gradi di liberta' calcolo il
                    % contributo alla matrice di rigidezza
                    Aval(k)=(Dy(i)*Dy(j)+Dx(i)*Dx(j))/(4*Area);
                    Ai(k)=ii;
                    Aj(k)=jj;
                    Mval=(1+(ii==jj))*Area/12;
                    k=k+1;
                else
                    Adval(kd)=(Dy(i)*Dy(j)+Dx(i)*Dx(j))/(4*Area);
                    Adi(kd)=ii;
                    Adj(kd)=-jj;
                    kd=kd+1;
                end
            end
            b(ii)=b(ii)+Area*f(xg,yg)/3.0; % calcolo del termine noto
        end
    end
end

A=sparse(Ai,Aj,Aval,Ndof,Ndof); % Assemblo la matrice di rigidezza usando il comando sparse: Aval contiene i valori, Ai gli indici riga e Aj gli indici colonna dei termini non nulli. La matride deve avere doimensioni NdofxNdof
M=sparse(Ai,Aj,Mval,Ndof,Ndof);
Ad=sparse(Adi(1:kd-1),Adj(1:kd-1),Adval(1:kd-1),Ndof,NNodof);

b=b-Ad*Rg;

b=add_Neumann(E,geom.pivot.Ne,pivot,P,@(x,y) -3*length(x),b);


x=A\b;

u=zeros(Nnode,1); % vettore soluzione in tutti i vertici
for i=1:Nnode
    if pivot(i)>0
        u(i)=x(pivot(i)); % Copio la soluzione calcolata nei gradi di liberta', negli altri nodi lascio il valore ZERO pari alla condizione di Dirichlet
    else
        u(i)=geom.input.BC.Values(geom.pivot.nodelist(i));
    end
end
figure(1)
trisurf(T, P(:,1),P(:,2),u); % Funzione MATLAB per eseguire la rappresentazione grafica della soluzione.
title('steady state solution')


% transitorio


x=zeros(Ndof,1);
u=zeros(Nnode,1); % vettore soluzione in tutti i vertici
for i=1:Nnode
    if pivot(i)>0
        u(i)=x(pivot(i)); % Copio la soluzione calcolata nei gradi di liberta', negli altri nodi lascio il valore ZERO pari alla condizione di Dirichlet
    else
        u(i)=geom.input.BC.Values(geom.pivot.nodelist(i));
    end
end

figure(2)
trisurf(T, P(:,1),P(:,2),u); % Funzione MATLAB per eseguire la rappresentazione grafica della soluzione.
title(['t=0'])
axis([0 1 0 1 0 0.1])
hold on

dt=0.001;

D=M+dt*A;

nf=1000;

for n=1:nf
    x=D\(dt*b+M*x);
    u=zeros(Nnode,1); % vettore soluzione in tutti i vertici
    for i=1:Nnode
        if pivot(i)>0
            u(i)=x(pivot(i)); % Copio la soluzione calcolata nei gradi di liberta', negli altri nodi lascio il valore ZERO pari alla condizione di Dirichlet
        else
            u(i)=geom.input.BC.Values(geom.pivot.nodelist(i));
        end
    end
    figure(2)
    trisurf(T, P(:,1),P(:,2),u); % Funzione MATLAB per eseguire la rappresentazione grafica della soluzione.
    axis([0 1 0 1 0 0.1])
    title(['t=',num2str(n*dt)])
    pause(0.01)
    hold off
end
