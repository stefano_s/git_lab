function [x, y]=global_coord(x_loc, y_loc, v1,v2,v3);

x1=v1(1);
y1=v1(2);
x2=v2(1);
y2=v2(2);
x3=v3(1);
y3=v3(2);

x=x1*(1-x_loc-y_loc)+x2*x_loc+x3*y_loc;
y=y1*(1-x_loc-y_loc)+y2*x_loc+y3*y_loc;