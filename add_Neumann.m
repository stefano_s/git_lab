function [b]=add_Neumann(E,pivotNe,pivot,P,hfun,b)

Ene=pivotNe(:,1);

for k=1:length(Ene)
    if pivotNe(k,2)==4
        V1=E(Ene(k),1);
        V2=E(Ene(k),2);
        Xv1=P(V1,:);
        Xv2=P(V2,:);
        dofV1=pivot(V1);
        if dofV1>0
            L=norm(Xv2-Xv1);
            I=L/6*(hfun(Xv1(1),Xv1(2))+2*hfun(0.5*(Xv1(1)+Xv2(1)),0.5*(Xv1(2)+Xv2(2))));
            b(dofV1)=b(dofV1)+I;
        end
        dofV2=pivot(V2);
        if dofV2>0
            L=norm(Xv2-Xv1);
            I=L/6*(hfun(Xv2(1),Xv2(2))+2*hfun(0.5*(Xv1(1)+Xv2(1)),0.5*(Xv1(2)+Xv2(2))));
            b(dofV2)=b(dofV2)+I;
        end
    end
end
